import requests
import re
import time
import json
import random

# 请求头信息，定义为全局方便重用
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36", # 替换为你的UA
    "Referer": "https://tieba.baidu.com/",
    "Cookie": 'PSTM=1620886678; BIDUPSID=2FD27C69E844F16E6C041C2C707D0B19; USER_JUMP=-1; st_key_id=17; 579096198_FRSVideoUploadTip=1; video_bubble579096198=1; wise_device=0; mo_originid=1; Hm_lpvt_7d6994d7e4201dd18472dd1ef27e6217=1663046713; rpln_guide=1; bottleBubble=1; Hm_lvt_287705c8d9e2073d13275b18dbd746dc=1675673104; Hm_lpvt_287705c8d9e2073d13275b18dbd746dc=1677113483; LONGID=579096198; BAIDUID=A04FA2D17613AF3FC0425AF5CB7FD4B2:FG=1; BDUSS=nN-R3FtZmwxTVF1VDdKNmJGMkdQeXpjOERXUDg0N2REZ2hzTEZGQlN6elU1TlZrSVFBQUFBJCQAAAAAAAAAAAEAAACGToQiX8zs1~fE9dPMv8nLoQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANRXrmTUV65kN; BDUSS_BFESS=nN-R3FtZmwxTVF1VDdKNmJGMkdQeXpjOERXUDg0N2REZ2hzTEZGQlN6elU1TlZrSVFBQUFBJCQAAAAAAAAAAAEAAACGToQiX8zs1~fE9dPMv8nLoQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANRXrmTUV65kN; tb_as_data=c83308b70ae30123a09f13958eadcf73085df2e33104fcad16754efe16481064ae5e497366afabbbb4415bc2f2054724d3d47298888d99c434dfb2e62b808d9f178dad531e77c51b5be24c11bb071f2221e4ab1083ef3c2e428604d3eca70a3416f78999dec3866972aa997b9c13f480; MCITY=-257%3A; STOKEN=258719b998fa616eb9db1b2e59eeb738276601c7393f3c0af138cf9f14c89047; H_WISE_SIDS=110085_256302_259642_269831_269904_271169_270102_272279_272461_272829_273120_273265_273301_272640_274077_272561_273981_275148_273924_275776_275941_275853_269610_276128_276250_274503_275907_275170_276420_276441_276572_275691_275590_276984_276987_276964_277030_276936_277313_277356; BDSFRCVID=7ZFOJeCmHRfWCq6qW1X2MehkGgKK0gOTHllnwYYpv4q8wrKVJeC6EG0Ptf8g0KubFTPRogKK0gOTH6KF_2uxOjjg8UtVJeC6EG0Ptf8g0M5; H_BDCLCKID_SF=tRAOoC-htKv0jbTg-tP_-4_tbh_X5-RLfbT2Mh7F5lONHJIxQqr2hPKbQNQ9W4Q325PHahkM5h7xOKQOXlo0yRLO5nbyKhbdQDcIQD5N3KJmo-P9bT3v5Du9Dmce2-biWb7M2MbdJpjP_IoG2Mn8M4bb3qOpBtQmJeTxoUJ25DnJhhCGe6KbDj3XDHtjq-Tta64DsDKhMnTHJRjYq4b_eT-lQ4nZKxJLWNrh-hOhJtjRhfP4XtosMU-syP4jKMRnWIJQWt3wy4J-M-D95n5v3xI8LNj405OTbIFO0KJcbRoPSMJ-hPJvyT8sXnO7tfnlXbrtXp7_2J0WStbKy4oTjxL1Db3JKjvMtgDtVJO-KKCaMCDljxK; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; Hm_lvt_98b9d8c2fd6608d564bf2ac2ae642948=1696640069; showCardBeforeSign=1; XFI=5efd4220-68b1-11ee-913b-079dca5079f6; XFCS=E83D8B4A8A095860F30C6E466E6A05BC6FE3EB978133F9ED73D0A9664FFB04BE; XFT=4X2zNmlzM7q+TzV34aYLuX22FRNWu15o4g3od/fF2Mw=; BAIDUID_BFESS=A04FA2D17613AF3FC0425AF5CB7FD4B2:FG=1; BDSFRCVID_BFESS=7ZFOJeCmHRfWCq6qW1X2MehkGgKK0gOTHllnwYYpv4q8wrKVJeC6EG0Ptf8g0KubFTPRogKK0gOTH6KF_2uxOjjg8UtVJeC6EG0Ptf8g0M5; H_BDCLCKID_SF_BFESS=tRAOoC-htKv0jbTg-tP_-4_tbh_X5-RLfbT2Mh7F5lONHJIxQqr2hPKbQNQ9W4Q325PHahkM5h7xOKQOXlo0yRLO5nbyKhbdQDcIQD5N3KJmo-P9bT3v5Du9Dmce2-biWb7M2MbdJpjP_IoG2Mn8M4bb3qOpBtQmJeTxoUJ25DnJhhCGe6KbDj3XDHtjq-Tta64DsDKhMnTHJRjYq4b_eT-lQ4nZKxJLWNrh-hOhJtjRhfP4XtosMU-syP4jKMRnWIJQWt3wy4J-M-D95n5v3xI8LNj405OTbIFO0KJcbRoPSMJ-hPJvyT8sXnO7tfnlXbrtXp7_2J0WStbKy4oTjxL1Db3JKjvMtgDtVJO-KKCaMCDljxK; delPer=0; PSINO=6; BA_HECTOR=2405a02005000lah8la481201iifd5k1p; ZFY=GSiF9xK5fOol97WVuAGQUlOOd7LnLqbVxCuGNVTgjLg:C; H_PS_PSSID=39319_39368_39393_39419_39412_39439_39481_39475_39463_39234_39487_26350_39430; BAIDU_WISE_UID=wapp_1697159476770_356; RT="z=1&dm=baidu.com&si=0f6ff8f1-011f-4930-be9b-58de9348b0de&ss=lnnwxl63&sl=0&tt=0&bcn=https%3A%2F%2Ffclog.baidu.com%2Flog%2Fweirwood%3Ftype%3Dperf"; Hm_lpvt_98b9d8c2fd6608d564bf2ac2ae642948=1697159479; ab_sr=1.0.1_M2YxNGJjYjI2MzEzYzUwYjg5ZmZkNmM0NWY0M2Y5ZWY0ZDA4MjMxNDY4YjI5MzNmMTQ5ODQwZTRlNGQ1YmVlOGI4YWEwMmZmNTI0ZTlkMDA0NDQ4YzcwNjY1NGEyZGJmMTY5MzM5ZjEyYmZiN2Y4MTAxZjcxMDJmOGY4NzFhMDBlYmVlMjFiMzNkODMwYjg2MzMxZWY0NDM0ZDZjOGY2NDhjYTVmYTUwM2EzNzI2YzM1Y2M0OWQ2NGZlOTg3ZmMw; st_data=388cdc95bb3c08bfc0efd59364378d09694409bb7a92294a5101266c53336427b8495bda7fa3f295a4bde10c395983925eba50bfb914f92b6a59f721f974f5bd9f670250a5295e1598d1a9dc425ca5ceffe9e977b306f184f58de39896015393f6f4e68cf081cd143e57121101603d9219a2522ed113b28cb8e29f437a6026c269eff8280d3b5900775f1f6c48ab4a7b; st_sign=4df60853'
}

# 从HTML中获取tbs参数和贴吧关注列表
def getTiebaInfo():
    try:
        url = "https://tieba.baidu.com/"

        # 发送请求，下载贴吧HTML页面
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        response.encoding = response.apparent_encoding

        html = response.text

        # 从HTML中提取tbs参数
        tbs = getTbs(html)

        # 从HTML中提取贴吧关注列表
        forums = getAllForums(html)

        return {"forums": forums, "tbs": tbs}
    except Exception as e:
        print("获取贴吧数据失败，原因:", e)
        return None

# 用正则表达示从HTML中提取参数 tbs 的值
def getTbs(html):
    # 正则表达式学得不太好，用得有点呆板，凑合用
    match = re.search(r'PageData.tbs = "(.*)";PageData.is_iPad', html)
    if match:
        tbs = match.group(0).split('"')[1]
        return tbs
    return None


# 获取关注的贴吧列表
def getAllForums(html):
    #  _.Module.use('spage/widget/forumDirectory', {"forums": [...],"directory": {}})
    match = re.search(r'{"forums":\[.*\],"directory"', html)
    if match:
        data = match.group(0)
        forums = json.loads(data[data.find('['):data.rfind("]")+1])
        return forums
    return None

# 一键签到
def tiebaOnekeySignin(tbs):    

    # 获取参数tbs失败则退出签到
    if not tbs:
        print("获取参数tbs失败，签到失败")
        return

    url = "https://tieba.baidu.com/tbmall/onekeySignin1"
    data = {
        "ie": "utf-8",
        "tbs": tbs
    }

    try:
        # 发送请求进行签到
        response = requests.post(url, data=data, headers=headers)
        response.raise_for_status()
        response.encoding = response.apparent_encoding

        # 分析响应结果 
        content = response.json()
        code = content.get("no")

        # 打印提示
        if code == 0:
            print("贴吧一键签到成功")
        elif code == 2280006:
            print("全部吧都已经签过了")
        else:
            print("一键签到失败，请重试")
    except Exception as e:
        print("一键签到发生错误：", e)

# 逐个吧签到
def tiebaSigninOneByOne(tiebaInfo):
    # 签到接口
    signin_url = "https://tieba.baidu.com/sign/add"
    tbs = tiebaInfo.get("tbs")

    # 统计结果
    success_count = 0
    fail_count = 0

    # 签到
    for forum in tiebaInfo.get("forums"):
        # 跳过已经签到的贴吧，减少请求次数，防止验证码
        is_sign = forum.get("is_sign")
        if is_sign == 1:
            continue

        # 构建请求数据
        forum_name = forum.get("forum_name")
        sigin_data = {
            "ie": "utf-8",
            "kw": forum_name,
            "tbs": tbs,
        }

        try:
            # 发送请求签到
            response = requests.post(
                url=signin_url, data=sigin_data, headers=headers)
            response.raise_for_status()
            response.encoding = response.apparent_encoding

            content = response.json()

            # 判断签到结果，打印消息
            if content.get("no") == 0:
                success_count += 1
                print("{}吧签到成功".format(forum_name))
            else:
                fail_count += 1
                print("{}吧签到失败，失败原因：{}".format(
                    forum_name, content.get("error")))
        except Exception as e:
            fail_count += 1
            print("Error: {}吧签到发生错误，{}".format(forum_name, e))

        # 随机睡眠1-5秒，防止弹验证码，自动化不追求速度，一切求稳
        second = random.randint(3, 8)
        time.sleep(second)

    print("本次签到成功%d个，失败%d个" % (success_count, fail_count))


# 主方法
def main():
    print("-----------百度贴吧开始签到-------------")
    tiebaInfo = getTiebaInfo()
    if tiebaInfo:
        tiebaOnekeySignin(tiebaInfo.get('tbs'))
    tiebaInfo = getTiebaInfo()
    if tiebaInfo:
        tiebaSigninOneByOne(tiebaInfo)
    else:
        print("签到失败")
    print("-----------百度贴吧签到结束-------------")


if __name__ == "__main__":
    main()